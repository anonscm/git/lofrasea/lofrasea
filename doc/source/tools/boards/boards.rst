.. _malicious-board:

Boards
======

Supported boards that used the firmware to study attacks.
Currently there is only one board supported.

.. toctree::
   :maxdepth: 2
   :caption: Supported boards:

   arduino-dragino

