Arduino dragino
===============

Supported functions:

- eavesdropping
- jamming:

  * deny
  * targeted
  * triggered (ongoing)


Load firmware:

.. code-block:: bash

   sudo arduino --upload jamming_trigger.ino --port /dev/ttyACM1

See outputs:

.. code-block:: bash

   minicom -D /dev/ttyACM1 -b 115200


