.. LOFRASEA documentation master file, created by
   sphinx-quickstart on Wed Dec 11 11:10:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LOFRASEA's documentation!
====================================

============
Introduction
============
The LOrawan FRamework for SEcurity Audit (LOFRASEA) is a project for research
and educational purposes.
The first objective is to build a small and portable testbed to study jamming attacks on a LoRaWAN network.
The second objective is to have a reproductible research and education
platform to study and evaluatte LoRaWAN security.

==========
Motivation
==========
It exist large scale testbed such as the FIT IoT-LAB initiative
(https://www.iot-lab.info/) that support LoRaWAN with several deployments.
But, we think that a small, portable, contrallable and reproducible tesbed is still needed for preliminary experiments to build dataset, to do first experiments and evaluation.

In this sense, a framework has been recently proposed, "WiSH-WalT: A Framework for Controllable andReproducible LoRa Testbed", that allows to build LoRaWAN testbed.
An other interesting framework called LAF has been proposed with a
focus on security.
So people interesting by other offensive tools should have a look at https://github.com/IOActive/laf.
Another framework called Chripotle about jamming and wormhole attacks has been recently
published in WISEC2020.
The code is available at https://github.com/seemoo-lab/chirpotle.

Compare to other recent frameworks LOFRASEA is more focus on the building of datasets to study and evaluate intrusion detection system algorithms.
As a perspective, in the future it could be used with Chirpotle or WiSH-Walt
that is two testbed interesting.

=======
Content
=======

In the *getting started* section you will find examples to get started with the
framework.
In the *tools* section you will find hw/sw related to exclusively to attacks,
analysis and countermeasures.
Finally in the *testbed* section you will find information that are related to
the LoRaWAN infrastructure, software to control a testbed, software deploy in
supported nodes and software to process results.

.. toctree::
   :maxdepth: 1

   getting-started/getting-started.rst
   tools/tools
   testbed/testbed

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
