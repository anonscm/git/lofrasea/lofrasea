.. LOFRASEA documentation master file, created by
   sphinx-quickstart on Wed Dec 11 11:10:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

End-Nodes
=========

End-nodes are used to build a testbed with LoRaWAN nodes.
For each node a firmware is adapted to the API of the Lofrasea framework.

Currently only one board is supported.

.. toctree::
   :maxdepth: 2
   :caption: Supported legitimate end-nodes:

   end-node-legitimate
   end-node-malicious

