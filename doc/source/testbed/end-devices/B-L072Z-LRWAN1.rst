B-L072Z-LRWAN1
==============


Dependancies:

* GNU arm toolchain: gcc-arm-none-eabi
* "GDB/OpenOCD"
* CMAKE >=3.6
* stlink

Fetch sources:

* git clone https://github.com/Lora-net/LoRaMac-node.git && cd LoRaMac-node
* git checkout master

Getting started:

Build the example app Lofrasea with the default paramters

.. code-block:: bash

   mkdir build
   cd build
   BOARD=B-L072Z-LRWAN1
   cmake -DCMAKE_TOOLCHAIN_FILE="../LoRaMac-node/cmake/toolchain-arm-none-eabi.cmake" -DAPPLICATION="Lofrasea" -DBOARD="$BOARD" ..
   make

The three default channels are:

   * 868.1 MHz
   * 868.3 MHz
   * 868.5 MHz

Load the firmware with stlink:

.. code-block:: bash

   st-info --probe
   st-flash write Lofrasea/Lofrasea-classA.bin 0x8000000

You can see on the UART outputs:

.. code-block:: bash

   minicom -D /dev/ttyACM0 -b 921600


Identity:

The main.c follow the development of the LoRaMac-node examples.                                                                                                                      
It has been modified for the Lofrasea application.                                                                                                                                   
For the identity please, in the LoRaMAC-node project, provision your keys in
the following file *LoRaMac-node/src/peripherals/soft-se/se-identify.h*  


The LoRaMAC-node application has been patch to add:

* log data related to the lofrasea connector API
* to fix to one channel for experiment in the file *regionEU868.h*:
  
   - #define EU868_NUMB_DEFAULT_CHANNELS                 1 //1 to fix one
   - #define EU868_NUMB_CHANNELS_CF_LIST                 0 // to have only one channel
