Testbed
=======

An overview of a Lofrasea testbed is depicted in the figure below.

.. figure:: overview.*

A Lofrasea testbed when running an expermiment is composed by several elements:

* one or several legetimate node
* one or several malicious node
* a LoRaWAN gateway
* a LoRaWAN network server
* a LoRaWAN application server
* a controller

A legitimate node call here *lofrasea-nl001* is composded by:

* a Single Board Computer (SBC) that allows to manage a LoRaWAN node
* a LoRaWAN node
* an antenna

A malicious node call here *lofrasea-ml001* is composed by:

* a Single Board Computer (SBC) that allows to manage a LoRaWAN node
* a LoRaWAN node
* an antenna

The LoRaWAN network is supported by only one provider which is the ChirpStack
project.
It allows to deploy a LoRaWAN gateway, to manage a LoRaWAN network with a network server and to manage application with the application server.
These elements of the LoRaWAN infrastructure can be deploy on the same hardware
or not.

The controller is basically a workstation that allows to connect to Lofrasea
nodes and to the LoRaWAN infrastructure at different points.

Finally, after an experiment a raw dataset can be post-process in order to
build a labeled dataset.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   lorawan-infra/lorawan-infra
   lofrasea-node/lofrasea-node
   end-devices/end-devices
   post-processing/post-processing

