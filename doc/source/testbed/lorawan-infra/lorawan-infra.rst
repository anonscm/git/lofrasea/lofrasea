LoRaWAN infrastructures
=======================

Every LoRaWAN network stack could be used with the LORAFREA project.
Indeed, the idea is to add probes without modifying existing solutions.

In order to not reinvent the wheel, we choose to use the ChirpStack project to set up our environnement with a
LoRaWAN Network Server.
It has the advantage to be fully functionnal, several applications could be
tested, and it will allow to manage real use cases. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   chirpstack

