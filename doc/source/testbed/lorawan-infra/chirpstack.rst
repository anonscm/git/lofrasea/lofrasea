ChirpStack
==========

Chirpstack (https://www.chirpstack.io/) is an open-source LoRaWAN Network Server stack.
With Chirpstack the Lofrasea testbed can be view as the figure below

.. figure:: testbed-overview-chirpstack.*

Chirpstack Gateway Full OS
--------------------------

Chirpstack offers several network deployment options.
We choose to use the ChirpStack Gateway OS full which provides a full
ChripStack Network Server and ChirpStack Application Server. 


Chirpstack and integration with Lofrasea
----------------------------------------

Chirpstack proposes several options to integrate external applications. 
At the network server level several technologies are supported: MQTT, AMQP,
HTTP, etc.

We choose to use the MQTT protocols to connect the Lofrasea framework to the
application server.
We also used MQTT protocol to get data from the gateway.


Chirpstack Gateway Lofrasea connector
-------------------------------------

The XXX allows to all data at the gateway level
The connector parse data and store them in a CSV file.


Chirpstack application server Lofrasea connector
------------------------------------------------

The XXX allows to get data related to the application we want to monitor.
The the connector parse data and store them in a CSV file.
