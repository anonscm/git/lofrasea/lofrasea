Getting Started
===============

In this chapter you will find examples to get started with the framework.
Currently there is only one example dealing with the deployment of a portable testbed.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
           
   testbed-lofrasea-example/testbed-lofrasea-example
