LOFRASEA Testbed example
========================

Introduction
************

TODO
+
TODO figure overview


LoRaWAN infrastructure
**********************

Hardware setup
--------------

The following section supposed that you have the following hardware:

- Raspberry pi 3 model B
- IC880A

TODO scheme with connection


Software setup
--------------

In order to not write two times the documentation, the best is to read the good
document done in the Chirpstack project (https://www.chirpstack.io/)[https://www.chirpstack.io/]

Steps to deploy the img:

.. code-block:: bash

   wget https://artifacts.chirpstack.io/downloads/chirpstack-gateway-os/raspberrypi/raspberrypi3/3.3.0-test.6/chirpstack-gateway-os-full-raspberrypi3-20200408112223.rootfs.wic.gz
   gunzip chirpstack-gateway-os-full-raspberrypi3-20200408112223.rootfs.wic.gz
   dd if=chirpstack-gateway-os-full-raspberrypi3-20200408112223.rootfs.wic of=/dev/mmcblk0 bs=4M


Connection to the WiFi AP ChirpStackAP (Read doc
https://www.chirpstack.io/gateway-os/use/getting-started/)
Connection to the board and first configuration:

.. code-block:: bash

   ssh admin@raspberrypi3.local
   sudo gateway-config

Set pin Reset to GPIO25 so set 25 (pin 22 raspi3) 
**Be carefull** last release set pin 5 by default so you have to change (if
possible) your setup cf.
https://github.com/brocaar/chirpstack-concentratord/issues/11

Configure to have json format for Gateway bridge

.. code-block:: bash

   sudo vi /etc/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml

and set the *marshaler="json"*

.. code-block:: bash

   sudo monit restart chirpstack-gateway-bridge


Chirpstack Network Server settings
----------------------------------

Chirpstack v3.10.0

When the full OS Gateway is running we can use the web interface at ip:8080
with login 'admin' and password 'admin'

* Create a network server with localhost:8000 (if not working use
  127.0.0.1:8000)
* Create a service profile called lofrasea and allows gateway meta-data
* Create a device profile called lofrasea-1.1 and allows OTAA
* Create an application called lofrasea-tests
* Create a device in the application lofrasea-tests

  * Device name: B-L072Z-LRWAN1


Lofrasea node
*************

Hardware setup
--------------

Software setup
--------------

Get image base:

.. code-block:: bash

   wget https://downloads.raspberrypi.org/raspios_lite_armhf_latest
   unzip raspios_lite_armhf_latest

Load on sdCard:

.. code-block:: bash

   dd bs=4M if=2020-08-20-raspios-buster-armhf-lite.img of=/dev/mmcblk0 conv=fsync


Add a file named *ssh* in the boot partition.

Provisionning image with package:
(sudo date -s "Thu Oct 15 15:30:00 UTC 2020")

.. code-block:: bash
   
   sudo apt install minicom vim tmux
   sudo apt install git
   sudo apt install python3-pip
   sudo pip3 install pyserial
   sudo apt install libusb-1.0 stlink-tools

Set hostname  for example "lofrasea-nl001.local"

.. code-block:: bash

   sudo raspi-config
   ping lofrasea-nl001.local

Add repository lofrasea

.. code-block:: bash

  git clone https://ptanguy@git.renater.fr/authscm/ptanguy/git/lofrasea/lofrasea.git 


Synchronization:

   sudo vim /etc/systemd/timesyncd.conf
   sudo timedatectl set-ntp true
   sudo timedatectl set-timezone Europe/Paris
   sudo systemctl daemon-reload

   sudo timedatectl set-time 15:58:30

To verify:
   systemctl status systemd-timesyncd.service


End-node legitimate
*******************

Hardware setup
--------------

B-L072Z-LRWAN1

Software setup
--------------

Dependancies:

* GNU arm toolchain: gcc-arm-none-eabi
* "GDB/OpenOCD"
* CMAKE >=3.6
* stlink

.. code-block:: bash

   mkdir build
   cd build
   BOARD=B-L072Z-LRWAN1
   cmake -DCMAKE_TOOLCHAIN_FILE="../LoRaMac-node/cmake/toolchain-arm-none-eabi.cmake" -DAPPLICATION="Lofrasea" -DBOARD="$BOARD" ..
   make

Load the firmware with stlink:

.. code-block:: bash

   st-info --probe
   st-flash write Lofrasea/Lofrasea-classA.bin 0x8000000

You can see on the UART outputs:

.. code-block:: bash

   minicom -D /dev/ttyACM0 -b 921600

End-node malicious
******************

Hardware setup
---------------

arduino-dragino

Software setup
--------------

TODO


Example of use
**************

Topology
--------

TODO

Launch an experiment
--------------------

TODO


Post-processing
---------------

TODO



