# The LoRaWAN framework for security audit

The LOrawan FRamework for SEcurity Audit (LOFRASEA) is a project for research
and educational purposes dedicated to jamming analysis.
It will also be in the futur used for LoRaWAN security evaluation but other
framework are really interestiong such as LAF et Chirpotle.

## Introduction

The first objective is to build a local testbed to study jamming attacks on a
LoRaWAN network.  Indeed, a local testbed is still needed for experimentation
and validation on little and specific use cases.  Moreover, open tested
specially for LoRaWAN network are deployed in a public environment which could
be an issue when testing such attacks.

Replicability is an important aspect of experimental testbed.  That is why, the
technical aspect to reproduce experimental results will be describe as much as
possible.


## Content of the repository

Below a description of the elements of the repository:

* doc: documentation
* tools: sources of stuff related to tools for auditing the network with
  jamming 
* testbed: all stuff related to the automation of the testbed


## Versioning and Changelog

See the [CHANGELOG](CHANGELOG) file.


## Contributing

See the [CONTRIBUTING](CONTRIBUTING) file.


## License

To be done:  [LICENSE](LICENSE) file.

## List of contributors

Acknowledgment to all contributors.
For details, look at the [CONTRIBUTORS](CONTRIBUTORS) file.

## Ongoing work and Roadmap

The idea is to improve the framework to allow testing & Auditing
LoRaWAN nodes and Networks with other security penetartion tools.  But,
recently an interesting framework called LAF has been proposed.  So people
interesting by other offensive tools should have a look at
https://github.com/IOActive/laf

Another framework called Chripotle about jamming attacks and wormhole has been recently
published in WISEC2020.
The code is available at https://github.com/seemoo-lab/chirpotle

Another work to do is to integrate power consumption measurement to study
depletion attacks.

For the roadmap, please refer to the Changelog and the TODO list below:

[] See LAF framework and possible integration
[] See Chripotle framework and possible integration
[] Add consumption profile capabilities (see branch consumption to integrate
stuff) [] See automation and possible integration with the Wish-Walt framework
(https://gricad-gitlab.univ-grenoble-alpes.fr/Drakkar-LIG/WalT/-/wikis/project).
The idea is to add controll capabilities to set end-devices parameters more
easily.

