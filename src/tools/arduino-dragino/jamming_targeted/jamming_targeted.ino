/*
   RadioLib SX127x Receive Example

   This example listens for LoRa transmissions using SX127x Lora modules.
   To successfully receive data, the following settings have to be the same
   on both transmitter and receiver:
    - carrier frequency
    - bandwidth
    - spreading factor
    - coding rate
    - sync word
    - preamble length

   Other modules from SX127x/RFM9x family can also be used.

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>

// SX1276 has the following connections:
// NSS pin:   10
// DIO0 pin:  2
// RESET pin: 9
// DIO1 pin:  3
SX1276 lora = new Module(10, 2, 9, 6);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1276 lora = RadioShield.ModuleA;

void setup() {
  Serial.begin(115200);

  Serial.print(F("[SX1276] Initializing ... "));
  // carrier frequency:           868.3 MHz
  // bandwidth:                   125 kHz
  // spreading factor:            7
  // coding rate:                 5
  // sync word:                   0x34 reserved for lorawan network
  // output power:                17 dBm (accepted range is -3 - 17 dBm)
  // current limit:               100 mA
  // preamble length:             8 symbols
  // amplifier gain:              0 (automatic gain control)
  int state = lora.begin(868.3, 125.0, 12, 5, 0x34, 17, 100, 8, 1);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }
}

void loop() {
  //Serial.print(F("[SX1276] Wait ... "));

  // you can receive data as an Arduino String
  // NOTE: receive() is a blocking method!
  //       See example ReceiveInterrupt for details
  //       on non-blocking reception method.

  size_t len = 16;
  byte rxData[len];
  static byte devAddr[4]; //Target DevAddr
  byte target[7]={0x50, 0x45, 0x44, 0x43, 0x42, 0x41, 0x40}; //DevEUI

  static bool wait_for_DevAddr;

  int n;

  // Receive data as byte array
  int state = lora.receive(rxData, len);

  if (state == ERR_NONE) {
    switch(rxData[0])
    {
      case 0:
        // Join request
        n = memcmp(&rxData[len-7],target,sizeof(byte)*7);
        if(n==0) {
          wait_for_DevAddr = true;
          Serial.print("Wait target ");
        } else {
          Serial.print("Ignore join ");
          // Packet no targeted
        }
        for(byte i=len-7;i<len;i++){
          Serial.print(rxData[i]);
          Serial.print("|");
        }
        break;
      case 64:
        // Data packet
        if(wait_for_DevAddr) {
          if(rxData[6] == 0) {
            // First data packet
            // Found target
            devAddr[0] = rxData[1];
            devAddr[1] = rxData[2];
            devAddr[2] = rxData[3];
            devAddr[3] = rxData[4];
            wait_for_DevAddr = false;
            Serial.print("Found target ");
          } else {
            Serial.print("Not target ");
            // Ignore packet
          }
        } else {
          n = memcmp(&rxData[1], devAddr, sizeof(byte)*4);
          if(n==0) {
            // Targeted packet
            state = lora.transmit("JammingJammingJammingJammingJammingJammingJammingJamming");

            if (state == ERR_NONE) {
              // the packet was successfully transmitted
              Serial.println(F("Jam target success "));
          
            } else if (state == ERR_PACKET_TOO_LONG) {
              // the supplied packet was longer than 256 bytes
              Serial.println(F("Jam too long "));
          
            } else if (state == ERR_TX_TIMEOUT) {
              // timeout occured while transmitting packet
              Serial.println(F("Jam timeout "));
          
            } else {
              // some other error occurred
              Serial.print(F("Jam failed, code "));
              Serial.println(state);
          
            }
            
          } else {
            Serial.print("Ignore jam ");
            // Untargeted
          }
          for(byte i=1;i<5;i++){
            Serial.print(rxData[i]);
            Serial.print("|");
          }
          Serial.print("  ");
          for(byte i=0;i<4;i++){
            Serial.print(devAddr[i]);
            Serial.print("|");
          }
        }
        break;
      default:
        // Unknown packet
        break;
    }
    // packet was successfully received
    Serial.println(F("success!"));
    
  } else if (state == ERR_RX_TIMEOUT) {
    // timeout occurred while waiting for a packet
    //Serial.println(F("timeout!"));
    //Serial.println();
  } else if (state == ERR_CRC_MISMATCH) {
    // packet was received, but is malformed
    Serial.println(F("CRC error!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);

  }
}
