/*
   RadioLib SX127x Receive Example

   This example listens for LoRa transmissions using SX127x Lora modules.
   To successfully receive data, the following settings have to be the same
   on both transmitter and receiver:
    - carrier frequency
    - bandwidth
    - spreading factor
    - coding rate
    - sync word
    - preamble length

   Other modules from SX127x/RFM9x family can also be used.

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>

// SX1276 has the following connections:
// NSS pin:   10
// DIO0 pin:  2
// RESET pin: 9
// DIO1 pin:  3
SX1276 lora = new Module(10, 2, 9, 6);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1276 lora = RadioShield.ModuleA;

void setup() {
  Serial.begin(115200);

  Serial.print(F("[SX1276] Initializing ... "));
  // carrier frequency:           868.3 MHz
  // bandwidth:                   125 kHz
  // spreading factor:            7
  // coding rate:                 5
  // sync word:                   0x34 reserved for lorawan network
  // output power:                17 dBm (accepted range is -3 - 17 dBm)
  // current limit:               100 mA
  // preamble length:             8 symbols
  // amplifier gain:              0 (automatic gain control)
  int state = lora.begin(868.3, 125.0, 12, 5, 0x34, 17, 100, 8, 0);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }
}

void loop() {
  //Serial.print(F("[SX1276] Wait ... "));

  // you can receive data as an Arduino String
  // NOTE: receive() is a blocking method!
  //       See example ReceiveInterrupt for details
  //       on non-blocking reception method.
  //String str;
  //int state = lora.receive(str);

  size_t len = 80; // 1 byte for the MAC Header, 5 bytes for the devAddr in the MAC payload
  byte rxData[len];
  byte target[len]={64, 0x50, 0x45, 0x44, 0x43, 0x42}; //MTYPE + devAddr

  int n;

  // you can also receive data as byte array
  
  int state = lora.receive(rxData, len);

  if (state == ERR_NONE) {
    n=memcmp(rxData,target,len);
    if(n>0){
     //Serial.print("Packet detected but not targeted");
    }
    else if(n<0){
     //Serial.print("Packet detected but not targeted");
    }
    else{
     Serial.print(F("Launch selective Jamming!"));
     lora.transmit("1");
     lora.standby();
    }
    
    //Serial.print(" packet: ");
    for(byte i=0;i<len;i++){
      Serial.print(rxData[i]);
      Serial.print("|");
    }
    
    // packet was successfully received
    Serial.println(F("success!"));
  } else if (state == ERR_RX_TIMEOUT) {
    // timeout occurred while waiting for a packet
    //Serial.println(F("timeout!"));
    //Serial.println();
  } else if (state == ERR_CRC_MISMATCH) {
    // packet was received, but is malformed
    Serial.println(F("CRC error!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);
  }
}
