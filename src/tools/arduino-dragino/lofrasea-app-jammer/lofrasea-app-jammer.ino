/*
   RadioLib SX127x Receive Example

   This example listens for LoRa transmissions using SX127x Lora modules.
   To successfully receive data, the following settings have to be the same
   on both transmitter and receiver:
    - carrier frequency
    - bandwidth
    - spreading factor
    - coding rate
    - sync word
    - preamble length

   Other modules from SX127x/RFM9x family can also be used.

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>

#define STOP_APP 0x01
#define START_JAM_TRIGGER 0x02

// SX1276 has the following connections:
// NSS pin:   10
// DIO0 pin:  2
// RESET pin: 9
// DIO1 pin:  3
SX1276 lora = new Module(10, 2, 9, 6);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1276 lora = RadioShield.ModuleA;

int state_ser;
int state_lora;
byte cmd;
bool app_start = false;

void setup() {
  Serial.begin(115200);

  Serial.print(F("START_"));
  // carrier frequency:           868.1 MHz
  // bandwidth:                   125 kHz
  // spreading factor:            7 (6 to 12)
  // coding rate:                 5
  // sync word:                   0x34 reserved for lorawan network
  // output power:                17 dBm (accepted range is -3 - 17 dBm)
  // current limit:               100 mA
  // preamble length:             8 symbols
  // amplifier gain:              0 (automatic gain control) 1 (highest) to 6 (lowest)
  state_ser = lora.begin(868.1, 125.0, 12, 5, 0x34, 17, 100, 6, 1);
  if (state == ERR_NONE) {
    Serial.println(F("SUCCESS"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }
}

void loop() {
        check_controller_cmd();
        if(cmd==START_JAM_TRIGGER){
                jam_trigger();
        }
        else if(cmd==STOP_APP){
                sleep(2);
        } else {
                sleep(2);
        }
}

void check_controller_cmd() {
        if (Serial.available() > 0) {
                // read the incoming byte:
                cmd = Serial.read();
        }
        if(incommingByte==0x01) {
                app_start = true;
        }
        else if(incommingByte==0x02) {
                app_start = false;
        }
}

void jam_trigger() {
  // start scanning current channel
  state_lora = lora.scanChannel();

  if (state_lora == PREAMBLE_DETECTED) {
      state_lora = lora.transmit("JammingJammingJammingJammingJammingJammingJammingJamming");
      if (state_lora == ERR_NONE) {
        // the packet was successfully transmitted
        Serial.println(F("JAM_TRIG_OK"));
      } else if (state_lora == ERR_PACKET_TOO_LONG) {
        // the supplied packet was longer than 256 bytes
        Serial.println(F("ERR_PACKET_TOO_LONG"));
      } else if (state_lora == ERR_TX_TIMEOUT) {
        // timeout occured while transmitting packet
        Serial.println(F("ERR_TX_TIMEOUT"));
      } else {
        // some other error occurred
        Serial.print(F("ERR_TRANSMIT "));
        Serial.println(state_lora);
      }
  } else if (state_lora == CHANNEL_FREE) {
    // timeout occurred while waiting for a packet
  } else {
    // some other error occurred
    Serial.print(F("ERR_PREAMBLE "));
    Serial.println(state_lora);
  }
}
