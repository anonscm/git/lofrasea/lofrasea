import os
import pandas as pd 

def gen_dataset(dataNode,dataJam,dataGW,dataAPP):
    # merge datasets according to timestamp 
    dataGW.index=dataGW['timestamp']
    dataJam.index=dataJam['timestamp']
    dataAPP.index=dataAPP['timestamp']

    tol = 10 #define tolerancein seconds

    # First merge
    dfTmp=pd.merge_asof(left=dataGW,right=dataJam,right_index=True,
            left_index=True,
            direction='nearest',
            suffixes=('_gw','_jam'),
            tolerance=tol)

    # Second merge
    df=pd.merge_asof(left=dfTmp,right=dataAPP,right_index=True,
            left_index=True,
            direction='nearest',
            suffixes=('_gwjam','_app'),
            tolerance=tol)

    return df

if __name__ == "__main__":

    pathRes = '~/tmp_recherche/datasets/desktop-24-07-2020'

    ## load inputs
    dataAPP = pd.read_csv(pathRes+'/raw/'+"testAPP.csv") 
    dataGW = pd.read_csv(pathRes+'/raw/'+"testGW.csv") 
    dataNode = pd.read_csv(pathRes+'/raw/'+"testNode.csv") 
    dataJam = pd.read_csv(pathRes+'/raw/'+"testJam.csv") 

    ## Create dataframe
    df=gen_dataset(dataNode,dataJam,dataGW,dataAPP)

    ## Save dataframe generate after post-processing
    df.to_hdf(pathRes+'/labeled/'+'test-dataset-lorawan-jam-trigger.h5', key='df', mode='w')
    df.to_csv(pathRes+'/labeled/'+'test-dataset-lorawan-jam-trigger.csv')

    ## Save metada
    with open(os.path.expanduser('~/tmp_recherche/datasets/desktop-24-07-2020'+'/'+'metadata.txt'),'w+') as f:
        f.write("--- Metadata testbed lofrasea ---\n")
        f.write("Date: 24-07-2020\n")
        f.write("Devices: 2\n")
        f.write("App DC: 30s\n")
        f.write("App payload: 50 bytes\n")
        f.write("Attacker: 1 Jam triggered 868.1MHz\n")
        f.write("Comments: 1 node with a fix channel at 868.1\n")
        f.close

