from network import LoRa
import socket
import machine
import time
import ubinascii
import struct
#import cbor

# initialise LoRa in LORA mode
# Please pick the region that matches where you are using the device:
# Asia = LoRa.AS923
# Australia = LoRa.AU915
# Europe = LoRa.EU868
# United States = LoRa.US915
# more params can also be given, like frequency, tx power and spreading factor
#lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868,sf=10, frequency=868100000,coding_rate = LoRa.CODING_4_5, bandwidth=LoRa.BW_125KHZ, preamble=8, public=True)
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)

# create an ABP authentication params
#dev_addr = struct.unpack(">l", ubinascii.unhexlify('05000000'))[0]
#nwk_swkey = ubinascii.unhexlify('2B7E151628AED2A6ABF7158809CF4F3C')
#app_swkey = ubinascii.unhexlify('2B7E151628AED2A6ABF7158809CF4F3C')

#OTAA
app_eui = ubinascii.unhexlify('ADA4DAE3AC12676B')
app_key = ubinascii.unhexlify('4c101d86fc305bad5258e146285f2103')
dev_eui = ubinascii.unhexlify('c7a885529ea9e7f6')


# Tips to fix one channel
lora.add_channel(0, frequency=868300000, dr_min=0, dr_max=5)
lora.add_channel(1, frequency=868300000, dr_min=0, dr_max=5)
lora.add_channel(2, frequency=868300000, dr_min=0, dr_max=5)

# join a network using ABP (Activation By Personalisation)
#lora.join(activation=LoRa.ABP, auth=(dev_addr, nwk_swkey, app_swkey))
lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0)

while not lora.has_joined():
    time.sleep(2.5)
    print('Not yet joined...')

# remove all the non-default channels: Not sure it is necessary
for i in range(3, 16):
    lora.remove_channel(i)

# create a raw LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
# DR SF    BW
# 0  SF12  125khz
# 1  SF11  125khz
# 2  SF10  125khz
# 3  SF9   125khz
# 4  SF10  125khz
# 5  SF7   125khz
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# Application parameters
APP_PERIOD=30 # period in second
APP_PAYLOAD_SIZE=50 # size of the target payload

#MSG_LOG="msg sent: frameCounter=%s"
MSG_ERROR="Error msg not sent"
MSG_SENT="MSG_SENT"

def make_msg():
    #msg['frameCounter']=frameCounter
    msg=[0]*(APP_PAYLOAD_SIZE) # padding according to application payload size parameters to have a payload_length equatl to framecounter + padding
    return bytes(msg)

while True:
    msg = make_msg()

    # send some data
    s.setblocking(True)
    try:
        s.send(msg)
        #currentLog = lora.stats() #return a namedtuple
        print(MSG_SENT)   
    except:
        print(MSG_ERROR)

    s.setblocking(False)

    time.sleep(APP_PERIOD)
