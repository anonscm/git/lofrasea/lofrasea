# -*- coding: utf-8 -*-

import os
import paho.mqtt.client as mqtt
import json
import base64
import binascii
import struct
from datetime import datetime
import csv

APP_HOST = "raspberrypi3.local"
APP_ID = "1"

APP_TOPIC = "application/" + APP_ID
APP_TOPIC_UP = APP_TOPIC + "/device/#"

CSV_HEADER={
        'timestamp',
        'time',
        'iat_app_nw',
        'iat_app_node',
        "applicationID",
        "applicationName",
        "deviceName",
        "devEUI",
        "gatewayID",
        "uplinkID",
        "name",
        "rssi",
        "loRaSNR",
        "location",
        "frequency",
        "dr",
        "adr",
        "fCnt",
        "fPort",
        "type",
        "error",
        'data'}

LOG_FILE='testAPP.csv'

END_NODES={} # key,val with key the devEUI and val the timestamp
LAST_NODE=None # devEUI of the last node

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(APP_TOPIC_UP)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    data={}
    try:
        payloadMQTT = json.loads(msg.payload)
    except Exception as e:
        print("Couldn't parse raw data: %s" % msg.payload)

    rxInfo = payloadMQTT['rxInfo'][0]
    txInfo = payloadMQTT['txInfo']

    del payloadMQTT['rxInfo']
    del payloadMQTT['txInfo']

    data.update(payloadMQTT)
    data.update(rxInfo)
    data.update(txInfo)

    write_data_log(data)

def compute_iat(timestamp,devEUI):
    global END_NODES
    global LAST_NODE
    iatNW=None
    iatNode=None

    # timestamp last packet received by the GW
    if LAST_NODE == None:
        prevPacketTime=timestamp
    elif LAST_NODE in END_NODES:
        prevPacketTime=END_NODES[LAST_NODE]
    else:
        prevPacketTime=timestamp

    # timestamp last packet per node received by the GW
    if devEUI in END_NODES:
        prevPacketNodeTime=END_NODES[devEUI] 
    else:
        prevPacketNodeTime=timestamp

    # Compute delta time in seconds
    iatNW = timestamp - prevPacketTime
    iatNode = timestamp - prevPacketNodeTime

    #Update globals
    END_NODES[devEUI]=timestamp
    LAST_NODE = devEUI

    return iatNW,iatNode

def write_header_log():
    with open(LOG_FILE, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writeheader()

def write_data_log(data):
    log={}

    # Set timestamp
    now = datetime.now()
    #timestamp = now.strftime("%d-%m-%Y %H:%M:%S")
    timestamp = datetime.timestamp(now)

    devEUI=data['devEUI']

    # Populate log dict
    iatNW,iatNode = compute_iat(timestamp,devEUI)

    log['iat_app_nw'] = iatNW
    log['iat_app_node'] = iatNode

    log['timestamp']=timestamp
    log.update(data)
    
    print("write log")
    print(log)
    with open(LOG_FILE,'a+',newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writerow(log)

if __name__ == '__main__':
    print("Starting client probe for Application Server Chirpstack")
    print("Logging APP traffic")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(APP_HOST)
    write_header_log()

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
    client.loop_forever()

