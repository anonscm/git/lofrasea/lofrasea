import csv, os
import serial
import time
from datetime import datetime

SER_DEV='/dev/ttyACM0'
CSV_HEADER={
        'timestamp',
        'jamInfo'}
LOG_FILE='testJam.csv'

def write_header_log():
    with open(LOG_FILE, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writeheader()

def write_log(data):
    log={}

    # Set timestamp
    now = datetime.now()
    log['timestamp']=datetime.timestamp(now)
    log['jamInfo']=data
    
    print("write log")
    print(log)
    with open(LOG_FILE,'a+',newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writerow(log)

if __name__ == "__main__":
    print("Starting client for attacker node")
    write_header_log()
    ser = serial.Serial(SER_DEV)
    print(ser.name)
    while True:
        nodeMsg = ser.readline()
        if nodeMsg:
            data=nodeMsg.decode().replace('\r\n',"")
            write_log(data)

