# -*- coding: utf-8 -*-

import os
import paho.mqtt.client as mqtt
import json
import base64
import binascii
import struct
from datetime import datetime
import csv

GW_HOST = "raspberrypi3.local"
GW_ID = "b827ebfffed5b60e"

GW_TOPIC = "gateway/" + GW_ID
GW_TOPIC_STATS = GW_TOPIC + "/event/stats"
GW_TOPIC_UP = GW_TOPIC + "/event/up"

NB_ERRORS = 0
NB_PACKETS = 0

## LoRWAN
MTYPE_MASK = 0xCF
MTYPE_JOIN_REQUEST = 0
MTYPE_JOIN_ACCEPT = 1
MTYPE_UNCONFIRMED_DATA_UP = 2
MTYPE_UNCONFIRMED_DATA_DOWN = 3
MTYPE_CONFIRMED_DATA_UP = 4
MTYPE_CONFIRMED_DATA_DOWN = 5
MTYPE_RFU = 6
MTYPE_PROPRIETARY = 7

CSV_HEADER={
        'timestamp',
        'time',
        'iat_gw_nw',
        'iat_gw_node',
        'frequency',
        'modulation',
        'bandwidth',
        'spreadingFactor',
        'codeRate',
        'polarizationInversion',
        'gatewayID',
        'timeSinceGPSEpoch',
        'rssi',
        'loRaSNR',
        'channel',
        'rfChain',
        'board',
        'antenna',
        'location',
        'fineTimestampType',
        'context',
        'uplinkID',
        'crcStatus',
        'mtype',
        'devAddr',
        'fCtrl',
        'fCnt',
        'fPort'}

LOG_FILE='testGW.csv'

END_NODES={} # key,val with key the devAddr and val the timestamp
LAST_NODE=None # devAddr of the last node

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(GW_TOPIC_STATS)
    client.subscribe(GW_TOPIC_UP)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    #print(msg.topic+" "+str(msg.payload))
    try:
        payloadMQTT = json.loads(msg.payload)
    except Exception as e:
        print("Couldn't parse raw data: %s" % msg.payload)

    if msg.topic == GW_TOPIC_STATS:
        compute_error(payloadMQTT)
    elif msg.topic == GW_TOPIC_UP:
        try:
            phyPayload = payloadMQTT['phyPayload'].encode('ascii')
            phyPayload = base64.decodebytes(phyPayload)
        except Exception as e:
            print("Couldn't decode")

        try:
            rxInfo = payloadMQTT['rxInfo']
        except Exception as e:
            print("Couldn't get rx info")

        try:
            txInfo = payloadMQTT['txInfo']
        except Exception as e:
            print("Couldn't get tx info")

        write_data_log(txInfo,rxInfo,phyPayload)

def compute_error(payloadMQTT):
    global NB_ERRORS
    global NB_PACKETS
    nb_packet =  payloadMQTT['rxPacketsReceived']
    nb_packet_ok = payloadMQTT['rxPacketsReceivedOK']
    #NB_ERRORS = NB_ERRORS + error
    current_errors = nb_packet - nb_packet_ok
    print("--- Stats GW ---")
    print("Errors= ", current_errors)
    NB_ERRORS = NB_ERRORS + current_errors
    NB_PACKETS = NB_PACKETS + nb_packet
    print("Total packets received: ", NB_PACKETS)
    print("Total errors: ", NB_ERRORS)
    ratio = (float(NB_ERRORS)/float(NB_PACKETS))*100
    print("Pourcentage d'erreur: ", ratio)

def lorawan_pkt_dissec(phyPayload):
    macHeader = phyPayload[0]
    macPayload = phyPayload[1:-4]
    mic = phyPayload[-4:]
    print("--- Dissec Rx packet ---")
    mtype=get_MType(macHeader)

    if mtype == MTYPE_UNCONFIRMED_DATA_UP:
        print("MType: Unconfirmed data up")
        lorawanHdr = macPayload[0:8]
        lorawanPayload = macPayload[8:]
        assert (len(lorawanHdr)+len(lorawanPayload) == len(macPayload)), "Problem lorawan packet header len"

        devAddr = get_devAddr(lorawanHdr)
        print("DevAddr: " + str(binascii.hexlify(devAddr)))

        fCtrl = get_fCtrl(lorawanHdr)
        print("Fctrl: " + str(binascii.hexlify(fCtrl)))

        fCnt = get_fCnt(lorawanHdr)
        print("FCnt: ", int.from_bytes(fCnt,byteorder='little'))
        #print("FCnt: ",fCnt)

        fPort = get_fPort(lorawanHdr)
        print("FPort: " + str(binascii.hexlify(fPort)))
    elif mtype == MTYPE_JOIN_REQUEST:
        print("MType: join request")
    elif mtype == MTYPE_JOIN_ACCEPT:
        print("MType: join request")
    elif mtype == MTYPE_UNCONFIRMED_DATA_DOWN:
        print("MType: Unconfirmed data down")
    elif mtype == MTYPE_CONFIRMED_DATA_UP:
        print("MType: confirmed data up")
    elif mtype == MTYPE_CONFIRMED_DATA_DOWN:
        print("MType: confirmed data down")
    elif mtype == MTYPE_RFU:
        print("MType: RFU")
    elif mtype == MTYPE_PROPRIETARY:
        print("MType: proprietaray")
    else:
        print("unknown mtype")

    return mtype,devAddr,fCtrl,fCnt,fPort

def get_MType(macHeader):
    mtype=(macHeader & MTYPE_MASK) >> 5
    return mtype

def get_RFU(macHeader):
    pass

def get_Major(macHeader):
    pass
    #major=(macHeader & MTYPE_MASK) >> 5
    #return major

def get_devAddr(header):
    devAddr=header[0:4]
    return devAddr

def get_fCtrl(header):
    data=header[4:5]
    return data

def get_fCnt(header):
    data=header[5:7]
    return data

def get_fOpts(header,fCnt):
    pass

def get_fPort(header):
    data=header[7:8]
    return data

def compute_iat(timestamp,devAddr):
    global END_NODES
    global LAST_NODE
    iatNW=None
    iatNode=None

    # timestamp last packet received by the GW
    if LAST_NODE == None:
        prevPacketTime=timestamp
    elif LAST_NODE in END_NODES:
        prevPacketTime=END_NODES[LAST_NODE]
    else:
        prevPacketTime=timestamp

    # timestamp last packet per node received by the GW
    if devAddr in END_NODES:
        prevPacketNodeTime=END_NODES[devAddr] 
    else:
        prevPacketNodeTime=timestamp

    # Compute delta time in seconds
    iatNW = timestamp - prevPacketTime
    iatNode = timestamp - prevPacketNodeTime

    #Update globals
    END_NODES[devAddr]=timestamp
    LAST_NODE = devAddr

    return iatNW,iatNode

def write_header_log():
    with open(LOG_FILE, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writeheader()

def write_data_log(txInfo,rxInfo,rxData):
    log={}
    loraInfo={}

    # Refactor txInfo and create loraInfo
    loraInfo = txInfo['loRaModulationInfo']
    del txInfo['loRaModulationInfo']
    
    # Get public information of lorawan frame
    mtype,devAddr,fCtrl,fCnt,fPort = lorawan_pkt_dissec(rxData) 
    lorawanInfo = {
            "mtype":mtype,
            "devAddr":devAddr.hex(),
            "fCtrl":fCtrl,
            "fCnt": int.from_bytes(fCnt,byteorder='little'),
            "fPort":fPort  
            }

    # Set timestamp
    now = datetime.now()
    #timestamp = now.strftime("%d-%m-%Y %H:%M:%S")
    timestamp = datetime.timestamp(now)

    # Populate log dict
    iatNW,iatNode = compute_iat(timestamp,str(devAddr.hex()))

    log['iat_gw_nw'] = iatNW
    log['iat_gw_node'] = iatNode

    log['timestamp']=timestamp
    log.update(txInfo)
    log.update(loraInfo)
    log.update(rxInfo)
    log.update(lorawanInfo)
    
    print("write log")
    print(log)
    with open(LOG_FILE,'a+',newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writerow(log)

if __name__ == '__main__':
    print("Starting client probe for gateway Chirpstack")

    print("Logging GW traffic")
    write_header_log()

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(GW_HOST)

    client.loop_forever()

