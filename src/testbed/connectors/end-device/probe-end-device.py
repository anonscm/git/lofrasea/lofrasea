import csv, os
import serial
import time
from datetime import datetime

# Serial default value
SER_DEV='/dev/ttyACM0'
BAUDRATE=921600

CSV_HEADER={
        'timestamp',
        'endDeviceInfo'}
LOG_FILE='testNode.csv'

def write_header_log():
    with open(LOG_FILE, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writeheader()

def write_log(data):
    log={}
    # Set timestamp
    now = datetime.now()
    #log['timestamp']=now.strftime("%d-%m-%Y %H:%M:%S")
    log['timestamp']=datetime.timestamp(now)
    log['endDeviceInfo']=data
    
    print("write log")
    print(log)
    with open(LOG_FILE,'a+',newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=CSV_HEADER)
        writer.writerow(log)


if __name__ == "__main__":
    print("Starting client for end-node")
    write_header_log()
    ser = serial.Serial(SER_DEV)
    ser.baudrate=BAUDRATE
    print("Serial port",ser.name)
    print("Serial baudrate",ser.baudrate)
    while True:
        nodeMsg = ser.readline()
        if nodeMsg:
            data=nodeMsg.decode().replace('\r\n',"")
            if data == "MSG_SENT":
                print(data)
                write_log(data)
